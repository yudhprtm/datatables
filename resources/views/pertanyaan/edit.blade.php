@extends('adminlte.master')

@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
          <h3 class="card-title">Edit Pertanyaan {{$index->id}}</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="/pertanyaan/{{$index->id}}" method="post">
            @csrf
            @method('PUT')
            <div class="card-body">
              <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $index->judul) }}" placeholder="Tulis judul">
                @error('judul')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="isi">Isi</label>
                <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', $index->isi) }}" placeholder="Tulis isi">
                @error('isi')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
        </div>
        <!-- /.card -->
        @endsection