@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Tabel Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif
            <a class="btn btn-info mb-2" href="/pertanyaan/create">Create New</a>
          <table class="table table-bordered">
            <thead>                  
              <tr>
                <th style="width: 10px">#</th>
                <th>Judul</th>
                <th>Isi</th>
                <th style="width: 40px">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($index as $key => $indexing)
                  <tr>
                  <td>{{ $key+1 }}</td>
                  <td>{{ $indexing->judul }}</td>
                  <td>{{ $indexing->isi }}</td>
                  <td style="display: flex">
                    <a href="/pertanyaan/{{$indexing->id}}" class="btn btn-info btn-sm">Show</a>
                    <a href="/pertanyaan/{{$indexing->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                    <form action="/pertanyaan/{{$indexing->id}}" method="post">
                      @csrf
                      @method('DELETE')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm" name="" id="">
                    </form>
                  </td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" align="center">Tidak ada data</td>
                  </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
</div>
</div>
@endsection