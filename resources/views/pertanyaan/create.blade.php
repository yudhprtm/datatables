@extends('adminlte.master')

@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Pertanyaan</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" action="/pertanyaan" method="post">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Tulis judul">
                @error('judul')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="isi">Isi</label>
                <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', '') }}" placeholder="Tulis isi">
                @error('isi')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
        </div>
        <!-- /.card -->
        @endsection