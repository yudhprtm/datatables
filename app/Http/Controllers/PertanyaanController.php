<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);
        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);

        // $index = new Pertanyaan;
        // $index->judul = $request['judul'];
        // $index->isi = $request['isi'];
        // $index->save();
        
        $index = Pertanyaan::create([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan');
    }

    public function index()
    {
        //$index = DB::table('pertanyaan')->get(); // select * from pertanyaan
        //dd($index);
        $index = Pertanyaan::all();
        return view('pertanyaan.index', compact('index'));
    }

    public function show($id)
    {
        $index = Pertanyaan::find($id);
        //$index = DB::table('pertanyaan')->where('id', $id)->first();
        //dd($index);
        return view('pertanyaan.show', compact('index'));
    }

    public function edit($id)
    {
        $index = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('index'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);
        // $index = DB::table('pertanyaan')
        //     ->where('id', $id)
        //     ->update([
        //         'judul' => $request['judul'],
        //         'isi' => $request['isi']
        //     ]);
        $index = Pertanyaan::where('id', $id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);
            return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan');
    }

    public function destroy($id)
    {
        // $index = DB::table('pertanyaan')
        // ->where('id', $id)
        // ->delete();
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil di hapus');
    }
}
