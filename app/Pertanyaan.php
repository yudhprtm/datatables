<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "Pertanyaan";
    public $timestamps = false;
    protected $fillable = ['judul', 'isi'];
}
